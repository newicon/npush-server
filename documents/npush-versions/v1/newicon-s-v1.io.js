/*! NEWICON PUSHER build:0.0.1, development. Copyright(c) 2012 NEWICON LTD */

/**
 * Krzysztof Stasiak
 * newicon-s.io
 * Copyright(c) 2012 NEWICON LTD <newicon@newicon.net>
 * GPL Licensed
 */

/**
 * Module dependencies.
 */

var express = require('express')
, stylus = require('stylus')
, nib = require('nib')
, sio = require('socket.io')
, databaseUrl = "ni_sockets" //'username:password@example.com:port/mydb'
, collections = ["messages_public", "messages_private", "accounts"]
, db = require("mongojs").connect(databaseUrl, collections)
, port = process.argv[2]
, qs = require('querystring');

var logToConsole = 1;

/**
 * App.
 */

var app = express.createServer();

/**
 * App configuration.
 */

app.configure(function () {
	app.use(stylus.middleware({
		src: __dirname + '/public', 
		compile: compile
	}));
	app.use(express.static(__dirname + '/public'));
	app.set('views', __dirname);
	app.set('view engine', 'jade');

	function compile (str, path) {
		return stylus(str)
		.set('filename', path)
		.use(nib());
	};
});

/**
 * App routes.
 */

app.get('/', function (req, res) {
	res.render('index', {
		layout: false
	});
});


/**
 * App listen.
 */

app.listen(port, function () {
	var addr = app.address();
	console.log('   app listening on http://' + addr.address + ':' + addr.port);
});

/**
 * Socket.IO server (single process only)
 */

var io = sio.listen(app)
, nicknames = new Object();

// socket.io settings

io.configure('production', function(){
	io.enable('browser client minification');  // send minified client
	io.enable('browser client etag');          // apply etag caching logic based on version number
	io.enable('browser client gzip');          // gzip the file
	io.set('log level', 1);                    // reduce logging
	io.set('transports', [                     // enable all transports (optional if you want flashsocket)
		'websocket'
	, 'flashsocket'
	, 'htmlfile'
	, 'xhr-polling'
	, 'jsonp-polling'
	]);
});

io.configure('development', function(){
  io.set('transports', ['websocket']);
});
  
nicknames = {};
  
// AUTHORIZATION

io.configure(function (){
	io.set('authorization', function (handshakeData, callback) {

		var _search_msg = ' { "api_key" : "' + handshakeData.query.k + '" } ';
		_search = JSON.parse(_search_msg);

		db.accounts.find(_search).forEach(function(err, doc) {

			if (!doc) {
				callback(null, false);
			}	
			else
			{
				handshakeData.client_room = doc['client_room'];
				handshakeData.client_wellcome_msg = doc['wellcome_message'];
				handshakeData.address = handshakeData.address.address;
				handshakeData.port = handshakeData.address.port;
				handshakeData.api_key = handshakeData.query.k;
				handshakeData.channel = handshakeData.query.c;
				handshakeData.user_id = handshakeData.query.id;
				handshakeData.user_name = handshakeData.query.name;
				handshakeData.user_room = handshakeData.query.room;
		
				if(!nicknames[handshakeData.client_room]) {
		    
					nicknames[handshakeData.client_room] = {};
				}
				if(!nicknames[handshakeData.client_room][handshakeData.channel]) {
		    
					nicknames[handshakeData.client_room][handshakeData.channel] = {};
				}		
				if(!nicknames[handshakeData.client_room][handshakeData.channel][handshakeData.user_room]) {
		    
					nicknames[handshakeData.client_room][handshakeData.channel][handshakeData.user_room] = {};
				}		
				callback(null, true);
			}
		});
	});
});

// SOCKETS AND EVENTS

function convert(name, value)
{
	var doc = {};
	doc[name] = value;
	return doc;
}
// make a data object from the socket object
function makeMsgData(msg, socket){
	var _time = new Date();
	return {
		user_id:socket.user_id,
		user_name:socket.nickname, 
		msg:msg, 
		room:socket.room, 
		channel:socket.channel,
		time:_time.getTime()
	};
}
// make a data object from the db 
// also see makeMsgData
function makeMsgDataDb(doc){
	return {
		nickname:doc['user']['name'], 
		msg:doc['message'], 
		room:doc['room'], 
		channel:'',
		time:doc['date']
	};
}



io.sockets.on('connection', function (socket) {
      
	// USER LOGIN AND ENVIRONMENT SETTINGS
  
	io.sockets.socket(socket.id).emit('announcement', socket.handshake.client_wellcome_msg);
    
	nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room][socket.handshake.user_id] = socket.nickname = socket.handshake.user_name;

	socket.api_key = socket.handshake.api_key;
	socket.channel = socket.handshake.channel;
	socket.client = socket.handshake.client_room;
	socket.path = socket.handshake.client_room + '/' + socket.handshake.channel + '/' + socket.handshake.user_room;
	socket.room = socket.handshake.user_room;
	socket.user_id = socket.handshake.user_id;
	socket.user_name = socket.handshake.user_name;

	socket.join(socket.path);
	io.sockets.in(socket.path).emit('announcement', socket.handshake.user_name + ' connected in room ' + socket.path);
    
	// SEND NEW USER LIST FOR USERS IN ROOM 
	io.sockets.in(socket.path).emit('usersroom', nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room]);  
    
	// SEND NEW USER LIST FOR USERS IN APP
	io.sockets.emit('usersapp', nicknames[socket.handshake.client_room]);
    
	// SEND NEW USER LIST FOR USERS IN ROOMS IN CHANNEL
	var rooms = io.sockets.manager.rooms;
	var _path = '/' + socket.client + '/' + socket.channel + '/';
      
	for(room in rooms) {
		if(room.search(_path) == 0)      
		{ 
			io.sockets.in(room.substr(1, room.length)).emit('userschannel', nicknames[socket.handshake.client_room][socket.handshake.channel]);
		}
	}
   
	// EVENTS START
    
	// USERS MESSAGES
      
	socket.on('message', function (msg) {
		var _data = makeMsgData(msg, socket);
		
		db.messages_public.save({
			"api_key": socket.api_key, 
			"room" : '/' + socket.handshake.client_room + '/' + socket.channel + '/' + socket.room + '/', 
			"user" : {
				"id" : socket.user_id, 
				"name" : socket.user_name
			} , 
			"date": _data.time, 
			"message" : msg,
			"client" : socket.handshake.address + ':' + socket.handshake.port
		});

		io.sockets.in(socket.path).emit('message', _data);
	});
    
	// BROADCAST MESSAGES
  
	socket.on('message boradcast rooms', function (msg, fn) {
      
		var _data = makeMsgData(msg, socket);
	  
		var rooms = io.sockets.manager.rooms;
		var _path = '/' + socket.client + '/' + socket.channel + '/';
      
		db.messages_public.save({
			"api_key": socket.api_key, 
			"room" : _path, 
			"user" : {
				"id" : socket.user_id, 
				"name" : socket.user_name
			} , 
			"date": _data.time, 
			"message" : msg,
			"client" : socket.handshake.address + ':' + socket.handshake.port,
		});      
      
		for(room in rooms) {
			if(room.search(_path) == 0) {
				io.sockets.in(room.substr(1, room.length)).emit('message', _data);		
			}
		}
	});
  
	socket.on('message broadcast app', function (msg, fn) {
      
		var _data = makeMsgData(msg, socket);
	  
		var rooms = io.sockets.manager.rooms;
		var _path = '/' + socket.client + '/';
		var _time = new Date();
      
		db.messages_public.save({
			"api_key": socket.api_key, 
			"room" : _path, 
			"user" : {
				"id" : socket.user_id, 
				"name" : socket.user_name
			} , 
			"date": _data.time, 
			"message" : msg,
			"client" : socket.handshake.address + ':' + socket.handshake.port,
		});       
      
		for(room in rooms) {
	  	  
			if(room.search(_path) == 0)
			{
				io.sockets.in(room.substr(1, room.length)).emit('message', _data);
			}
		}
	});  
  
	socket.on('message broadcast channel', function (channel, msg, fn) {
      
		var _data = makeMsgData(msg, socket);
	  
		var rooms = io.sockets.manager.rooms;
		var _path = '/' + socket.client + '/' + channel + '/';
		var _time = new Date();
      
		db.messages_public.save({
			"api_key": socket.api_key, 
			"room" : _path, 
			"user" : {
				"id" : socket.user_id, 
				"name" : socket.user_name
			} , 
			"date": _data.time, 
			"message" : msg,
			"client" : socket.handshake.address + ':' + socket.handshake.port,
		});       
      
		for(room in rooms) {
	  
			if(room.search(_path) == 0)
			{
				io.sockets.in(room.substr(1, room.length)).emit('message', _data);
			}
		}
	});  
	
	// READ TIME OF LAST ACTIVITY IN MS FROM DB AND SEND UNREAD MESSAGES
  
	socket.on('public_activity_ms', function (_period, fn) {
           
		var _search_time = ' { "users_activity.' + socket.user_id + './' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : {"$exists": true}, "api_key" : "' + socket.api_key + '" } ';
    
		_search = JSON.parse(_search_time);
    
		db.accounts.find(_search).forEach(function(err, doc) {
	
			if (!doc) {
				return;
			}
			else
			{
				var _time = new Date();
				socket.user_last_activity = _time.getTime() - _period;
	
				var _time_info = new Date(socket.user_last_activity*1);
				io.sockets.socket(socket.id).emit('announcement', 'last activity ' + _time_info);
 
				// SEARCH FOR MESSAGES FOR ROOM
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/' + socket.room + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});
		
				// SEARCH FOR MESSAGES FOR CHANNEL
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});
		
				// SEARCH FOR MESSAGES FOR APP
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});		
			}
		});
	});  	
  
	// READ TIME OF LAST ACTIVITY FROM DB AND SEND UNREAD MESSAGES
  
	socket.on('public_activity', function (nick, fn) {
           
		var _search_time = ' { "users_activity.' + socket.user_id + './' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : {"$exists": true}, "api_key" : "' + socket.api_key + '" } ';
    
		_search = JSON.parse(_search_time);
    
		db.accounts.find(_search).forEach(function(err, doc) {
	
			if (!doc) {
				return;
			}
			else
			{
				socket.user_last_activity = doc['users_activity'][socket.user_id]['/' + socket.client + '/' + socket.channel + '/' + socket.room + '/']['date'];
				var _time = new Date(socket.user_last_activity*1);
				io.sockets.socket(socket.id).emit('announcement', 'last activity ' + _time);
 
				// SEARCH FOR MESSAGES FOR ROOM
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/' + socket.room + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});
		
				// SEARCH FOR MESSAGES FOR CHANNEL
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/' + socket.channel + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});
		
				// SEARCH FOR MESSAGES FOR APP
				var _search_msg = ' { "date" : { "$gt" : ' + socket.user_last_activity + ' }, "room" : "/' + socket.client + '/" } ';				
				_search = JSON.parse(_search_msg);

				db.messages_public.find(_search).forEach(function(err, doc) {

					if (!doc) {
						return;
					}	
					else
					{
						io.sockets.socket(socket.id).emit('message', makeMsgDataDb(doc));
					}
				});		
			}
		});
	});  

	// DISCONNECT AND SAVE TIME OF EVENT IN DB

	socket.on('disconnect', function () {
		if (!socket.nickname) return;
    
		var _time = new Date();	
    
		var _date = "users_activity." + socket.user_id + ".date";
		var _room = "users_activity." + socket.user_id + ".room";
		var _date = convert(_date, {
			$exists: true
		});
		var _update_time = convert(_date, _time.getTime());
		var _update_room = convert(_room, socket.room);
    
		var _search_json = '{ "api_key" : "' + socket.api_key + '" }';
		var _update_json = '{ "users_activity.' + socket.user_id + '.date" : "' + _time.getTime() + '", \n\
			    "users_activity.' + socket.user_id + './' + socket.client + '/' + socket.channel + '/' + socket.room + '/.date" : "' + _time.getTime() + '" } '; //, \n\

		_search = JSON.parse(_search_json);
		_update = JSON.parse(_update_json);
    	
		db.accounts.update(_search, {
			$set : _update
		} , {
			upsert : true
		} );    

		delete nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room][socket.user_id];
		io.sockets.in(socket.path).emit('announcement', socket.nickname + ' disconnected');
	
		//io.sockets.in(socket.path).emit('nicknames', nicknames[socket.handshake.client_room ]);
		io.sockets.in(socket.path).emit('usersroom', nicknames[socket.handshake.client_room][socket.handshake.channel][socket.handshake.user_room]);  
		io.sockets.emit('usersapp', nicknames[socket.handshake.client_room]);
		// SEND NEW USER LIST FOR USERS IN ROOMS IN CHANNEL
		var rooms = io.sockets.manager.rooms;
		var _path = '/' + socket.client + '/' + socket.channel + '/';

		for(room in rooms) {

			if(room.search(_path) == 0)      
			{ 
				io.sockets.in(room.substr(1, room.length)).emit('userschannel', nicknames[socket.handshake.client_room][socket.handshake.channel]);
			}
		} 
	});
});

app.post('/api/channel/:c/room/:r/api/:k/t/:t/suid/:suid/suname/:sun', function (req, res) {
	
	// :c - destination channel
	// :r - destination room
	// :k - api key
	// :t - time stamp
	// :suid - sender user id
	// :sun - sender user name
	
	// EXAMPLE:
	// curl -H "Content-Type: application/post" 
	//		-d '{"object":"helloworld"}' 
	//		"http://push.vm.newicon.net:80/api/channel/ch1/room/room1/api/ab3245cbeaf456244abcdfa/t/234324/suid/1/suname/me"

	if(req.method == 'POST') {
		
		// DATA
		
		var body ='';
		var _time_server = new Date();
		
		req.on('data', function (data) {
            body += data;
        });		
	
        req.on('end', function () {
			
			var _search_msg = ' { "api_key" : "' + req.params.k + '" } ';
			_search = JSON.parse(_search_msg);			
			
			db.accounts.find(_search).forEach(function(err, doc) {			
				
				if (!doc) {
					
					res.send('AUTH_ERROR');
					return;
				} else {
			
					var _path = doc['client_room'] + '/' + req.params.c + '/' + req.params.r;
													
					try {
						_msg_object = JSON.parse(body);
					} 
					catch (e) {
						_msg_object = body
					}
					
					try {
						
						_time_stamp = parseInt(req.params.t);
					} catch(e) {
						
						_time_stamp = req.params.t;
					}					

					var data = {
						user_id:req.params.suid,
						user_name:req.params.sun, 
						msg: _msg_object, 
						room:req.params.sur, 
						channel:req.params.suc,
						time:_time_server.getTime()	
					}
					
					db.messages_public.save({
						"api_key": req.params.k, 
						"room" : '/' + doc['client_room']+ '/' + req.params.c + '/' + req.params.r + '/', 
						"user" : {
							"id" : req.params.suid, 
							"name" : req.params.sun
						} , 
						"date": _time_server.getTime(), 
						"message" : _msg_object,
						"client" : req.connection.remoteAddress + ':' + req.connection.remotePort
					});										
					
					io.sockets.in(_path).emit('message', data);
					
					res.send('OK');
					return;
				}
			});				
        });
	}
});
