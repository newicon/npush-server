/**
 * NEWICON NPUSH SERVER build: 3.0.0, Copyright(c) 2013 NEWICON LTD
 * 
 * @author	Krzysztof Stasiak <chris.stasiak@newicon.net>
 * @name	npush-s.js
 * 
 * Copyright(c) 2013 NEWICON LTD <newicon@newicon.net>
 * GPL Licensed
 *
 * Proper command line to launch n:push
 *  
 * Example command:
 * $ NODE_ENV=production node npush-s.js 8080 10
 * 
 * Description:
 * $ -environement- -node- -script_name- -port- -instances-
 * 
 * REQUIREMENTS: MongoDB (+schema), RedisStore (settings in worker options)
 * 
 * @see README file
 * 
 */

var cluster = require('cluster'),
	npush	= require('npush');

// command line options : size is number of instances, port is TCP port
var size = process.argv[3] || 1,
	port = process.argv[2] || 8080;

if(cluster.isMaster) {
	
	/** Options for master **/	
	var options = {
		'startInstancesNumber' : size
	}
	/** Create MASTER instance, listen workers and activate stats **/	
	var master = npush.createMaster(options).listenWorkers().broadcastStats();
  
} else {
	
	/** Options for worker **/
	var options = {
		'id' : cluster.worker.id,
		'startInstancesNumber' : size,
		'allowGlobalTraffic' : true,
		'MongoDB' : { 
			'url' : "localhost:27017/ni_sockets", //'username:password@localhost:27017/ni_sockets'
			'collections' : ["accounts", "apis"]
		},
		'RedisServer' : { 
			'redisClient' : { 'port' : 6379, 'host' : 'localhost'} 
		},
		'NPUSH_SERVER_STATUS_APIKEY'  : '12345678987654321',	// Private Server and...
		'NPUSH_SERVER_CONTROL_APIKEY' : '12345678987654321'		// Cluster Keys for management		
	};
	
	/** Create WORKER instance and listen master **/
	var worker = npush.createWorker(port, options).listenMaster();
}